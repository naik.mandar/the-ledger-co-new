public class Balance {
    private String nameOfBank;
    private String nameOfBorrower;
    private int emiNo;

    public Balance(String nameOfBank, String nameOfBorrower, int emiNo) {
        this.nameOfBank = nameOfBank;
        this.nameOfBorrower = nameOfBorrower;
        this.emiNo = emiNo;
    }

    public String getNameOfBank() {
        return nameOfBank;
    }


    public String getNameOfBorrower() {
        return nameOfBorrower;
    }

    public int getEmiNo() {
        return emiNo;
    }

    @Override
    public String toString() {
        return "Balance{" +
                "nameOfBank='" + nameOfBank + '\'' +
                ", nameOfBorrower='" + nameOfBorrower + '\'' +
                ", noOfEMILeft=" + emiNo +
                '}';
    }
}

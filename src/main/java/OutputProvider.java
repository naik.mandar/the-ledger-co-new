import java.io.FileNotFoundException;
import java.util.List;

public class OutputProvider {
    Service service = new Service();
    List<Output> outputList;

    public void printOutput(String path) throws FileNotFoundException {
        outputList = service.getOutput(path);
        for (Output output : outputList) {
            System.out.println(output);
        }
    }
}

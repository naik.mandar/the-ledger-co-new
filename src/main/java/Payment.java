public class Payment {

    private String nameOfBank;
    private String nameOfBorrower;
    private double lumpSumAmount;
    private int emiNumber;

    public Payment(String nameOfBank, String nameOfBorrower, double lumpSumAmount, int emiNumber) {
        this.nameOfBank = nameOfBank;
        this.nameOfBorrower = nameOfBorrower;
        this.lumpSumAmount = lumpSumAmount;
        this.emiNumber = emiNumber;
    }

    public String getNameOfBank() {
        return nameOfBank;
    }

    public String getNameOfBorrower() {
        return nameOfBorrower;
    }

    public double getLumpSumAmount() {
        return lumpSumAmount;
    }

    public int getEmiNumber() {
        return emiNumber;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "nameOfBank='" + nameOfBank + '\'' +
                ", nameOfBorrower='" + nameOfBorrower + '\'' +
                ", lumpSumAmount=" + lumpSumAmount +
                ", emiNumber=" + emiNumber +
                '}';
    }
}

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Service {
    InputReader inputReader = new InputReader();

    public double amountPaid(Balance balance) {
        double amountPaid = 0;
        //check if borrower has made any lump sum payment before the emi number of input
        if(hasMadeLumpSumPayment(balance.getNameOfBorrower(), balance.getEmiNo())) {
            amountPaid = amountPerInstallment(balance.getNameOfBorrower()) * balance.getEmiNo()
                    + getPaymentDetails(balance.getNameOfBorrower()).getLumpSumAmount();
            } else {
               amountPaid = amountPerInstallment(balance.getNameOfBorrower()) * balance.getEmiNo();
        }
        return amountPaid;
    }

    public int NoOfInstallmentsLeft(Balance balance) {
        int noOfInstallmentLeft = 0;
        //check if borrower has made any lump sum payment before the emi number of input
        if(hasMadeLumpSumPayment(balance.getNameOfBorrower(), balance.getEmiNo())) {
            int reducedInstallments = (int)Math.floor(getPaymentDetails(balance.getNameOfBorrower())
                    .getLumpSumAmount() / amountPerInstallment(balance.getNameOfBorrower()));

            noOfInstallmentLeft = totalNoOfInstallments(balance.getNameOfBorrower()) - balance.getEmiNo()
                    - reducedInstallments;
        } else {
            noOfInstallmentLeft = totalNoOfInstallments(balance.getNameOfBorrower()) - balance.getEmiNo();
        }
        return noOfInstallmentLeft;
    }

    public double totalAmountPayable(String nameOfBorrower) {
        double totalAmountPayable = 0;
        totalAmountPayable =  getLoanDetails(nameOfBorrower).getPrincipal()
                + (getLoanDetails(nameOfBorrower).getPrincipal()
                / 100 * getLoanDetails(nameOfBorrower).getRateOfInterest()
                * getLoanDetails(nameOfBorrower).getNoOfYears());
        return totalAmountPayable;
    }

    public int totalNoOfInstallments(String nameOfBorrower) {
        int totalNoOfInstallment = 0;
        totalNoOfInstallment = getLoanDetails(nameOfBorrower).getNoOfYears() * 12;
        return totalNoOfInstallment;
    }

    public double amountPerInstallment(String nameOfBorrower) {
        double amountPerInstallment = 0;
        amountPerInstallment = Math.ceil(totalAmountPayable(nameOfBorrower)
                / totalNoOfInstallments(nameOfBorrower));
        return amountPerInstallment;
    }

    public Loan getLoanDetails(String borrowerName) {
        for (Loan loan : inputReader.getLoanRecord()) {
            if (loan.getNameOfBorrower().equals(borrowerName)) {
                return loan;
            }
        }
        return null;
    }

    public Payment getPaymentDetails(String borrowerName) {
        for (Payment payment : inputReader.getPaymentRecord()) {
            if (payment.getNameOfBorrower().equals(borrowerName)) {
                return payment;
            }
        }
        return null;
    }

    public boolean hasMadeLumpSumPayment(String borrowerName, int emiNo) {
        return getPaymentDetails(borrowerName) != null
                && getPaymentDetails(borrowerName).getEmiNumber() <= emiNo;
    }

    public List<Output> getOutput(String path) throws FileNotFoundException {

        List<Output> outputList = new ArrayList<>();

        inputReader.readInputFromTextFile(path);

        for (Balance balance : inputReader.getBalanceRecord()) {
            Output output = new Output(balance.getNameOfBank(), balance.getNameOfBorrower(), (long) amountPaid(balance), NoOfInstallmentsLeft(balance));
            outputList.add(output);
        }
        return outputList;
    }
}

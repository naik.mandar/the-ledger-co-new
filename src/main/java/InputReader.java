import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputReader {

    private final List<Loan> loanRecord = new ArrayList<>();
    private final List<Balance> balanceRecord = new ArrayList<>();
    private final List<Payment> paymentRecord = new ArrayList<>();

    public void readInputFromTextFile (String path) throws FileNotFoundException {
        File file = new File(path);
        try (Scanner in = new Scanner(file)) {

            String command;

            while (in.hasNextLine()) {
                command = in.next();
                if (command.equals("LOAN")) {

                    Loan loan = new Loan(in.next(), in.next(), in.nextInt(), in.nextInt(), in.nextInt());

                    loanRecord.add(loan);
                }

                if (command.equals("PAYMENT")) {

                    Payment payment = new Payment(in.next(), in.next(), in.nextInt(), in.nextInt());

                    paymentRecord.add(payment);
                }

                if (command.equals("BALANCE")) {

                    Balance balance = new Balance(in.next(), in.next(), in.nextInt());

                    balanceRecord.add(balance);
                }
                in.nextLine();
            }
        }
    }

    public List<Loan> getLoanRecord() {
        return loanRecord;
    }

    public List<Balance> getBalanceRecord() {
        return balanceRecord;
    }

    public List<Payment> getPaymentRecord() {
        return paymentRecord;
    }
}

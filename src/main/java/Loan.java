public class Loan {
    private String nameOfBank;
    private String nameOfBorrower;
    private double principal;
    private int noOfYears;
    private int rateOfInterest;

    public Loan(String nameOfBank, String nameOfBorrower, double principal, int noOfYears, int rateOfInterest) {
        this.nameOfBank = nameOfBank;
        this.nameOfBorrower = nameOfBorrower;
        this.principal = principal;
        this.noOfYears = noOfYears;
        this.rateOfInterest = rateOfInterest;
    }

    public String getNameOfBank() {
        return nameOfBank;
    }

    public String getNameOfBorrower() {
        return nameOfBorrower;
    }

    public double getPrincipal() {
        return principal;
    }

    public int getNoOfYears() {
        return noOfYears;
    }

    public int getRateOfInterest() {
        return rateOfInterest;
    }

    @Override
    public String toString() {
        return "Loan{" +
                "nameOfBank='" + nameOfBank + '\'' +
                ", nameOfBorrower='" + nameOfBorrower + '\'' +
                ", principal=" + principal +
                ", noOfYears=" + noOfYears +
                ", rateOfInterest=" + rateOfInterest +
                '}';
    }
}

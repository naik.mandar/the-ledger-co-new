public class Output {
    String bankName;
    String nameOfBorrower;
    long amountPaid;
    int noOfEMILeft;

    public Output(String bankName, String nameOfBorrower, long amountPaid, int noOfEMILeft) {
        this.bankName = bankName;
        this.nameOfBorrower = nameOfBorrower;
        this.amountPaid = amountPaid;
        this.noOfEMILeft = noOfEMILeft;
    }

    @Override
    public String toString() {
        return  bankName + " " + nameOfBorrower + " " + amountPaid + " " + noOfEMILeft ;
    }
}

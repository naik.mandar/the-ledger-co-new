import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import java.io.FileNotFoundException;

@ExtendWith(MockitoExtension.class)
class InputReaderTest {

    InputReader inputReader;

    @BeforeEach
    void setUp() throws FileNotFoundException {
        inputReader = new InputReader();
        inputReader.readInputFromTextFile("D:\\idea projects\\crud-operations-demo\\ledger-co\\src\\test\\java\\testInput.txt");
    }

    @Test
    void givenInputFile_whenGetLoanRecord_ReturnLoanRecord() {
        Assertions.assertEquals("Dale", inputReader.getLoanRecord().get(0).getNameOfBorrower());
        Assertions.assertEquals("IDIDI", inputReader.getLoanRecord().get(0).getNameOfBank());
        Assertions.assertEquals(5000, inputReader.getLoanRecord().get(0).getPrincipal());
        Assertions.assertEquals(1, inputReader.getLoanRecord().get(0).getNoOfYears());
        Assertions.assertEquals(6, inputReader.getLoanRecord().get(0).getRateOfInterest());
    }

    @Test
    void givenInputFile_whenGetBalanceRecord_ReturnBalanceRecord() {
        Assertions.assertEquals("Dale", inputReader.getBalanceRecord().get(0).getNameOfBorrower());
        Assertions.assertEquals("IDIDI", inputReader.getBalanceRecord().get(0).getNameOfBank());
        Assertions.assertEquals(3, inputReader.getBalanceRecord().get(0).getEmiNo());
    }

    @Test
    void givenInputFile_whenGetPaymentRecord_ReturnPaymentRecord() {
        Assertions.assertEquals("Dale", inputReader.getPaymentRecord().get(0).getNameOfBorrower());
        Assertions.assertEquals("IDIDI", inputReader.getPaymentRecord().get(0).getNameOfBank());
        Assertions.assertEquals(1000, inputReader.getPaymentRecord().get(0).getLumpSumAmount());
        Assertions.assertEquals(5, inputReader.getPaymentRecord().get(0).getEmiNumber());
    }
}
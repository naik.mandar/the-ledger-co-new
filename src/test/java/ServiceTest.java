import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileNotFoundException;

@ExtendWith(MockitoExtension.class)
class ServiceTest {

    Service service;

    @BeforeEach
    void setUp() throws FileNotFoundException {
    }

    @Test
    void amountPaid() {
    }

    @Test
    void noOfInstallmentsLeft() {
    }

    @Test
    void totalAmountPayable() {
    }

    @Test
    void totalNoOfInstallments() {
    }

    @Test
    void amountPerInstallment() {
    }

    @Test
    void getLoanDetails() {
    }

    @Test
    void getPaymentDetails() {
    }

    @Test
    void hasMadeLumpSumPayment() {
    }

    @Test
    void getOutput() {
    }
}